package StepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import PageObjects.editrestrictions;

import java.awt.RenderingHints.Key;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes.Name;

public class Restrictionpage {
	public static WebDriver driver;
	editrestrictions ms;

	public Restrictionpage() {
		driver = Hooks.driver;
	}

	@Given("^I navigate to the (.+?)$")
	public void iNavigateToThe(String url) {
		driver.get(url);
		// Assert.assertTrue("My Store".equalsIgnoreCase(driver.getTitle()));
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		ms = new editrestrictions(driver);

	}

	@When("^I Click continue$")
	public void iClickOnlogin() {
		ms.clicklogin();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@When("I Enter the username (.+?)$")
	public void iEnterusername(String Tasks) {
		ms.enterusername(Tasks);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@When("^I Click submit$")
	public void iClickOnsubmit() {
		ms.clicksubmit();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@When("I Enter the password (.+?)$")
	public void iEnterpassword(String Tasks) {
		ms.enterpassword(Tasks);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@When("^I Click login$")
	public void iClicklogin() throws InterruptedException {
		ms.clicksubmit();
		

		Thread.sleep(5000);
	}

	@When("^I Click profile$")
	public void iClickprofile() throws InterruptedException {
		ms.clickprofile();
		
		Thread.sleep(5000);
	}

	@When("^I Click confluence$")
	public void iClickconfluence() throws InterruptedException {
		ms.clickconfluence();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@When("^I Click page")
	public void iClickconpage() throws InterruptedException {
		ms.clickpage();
		Thread.sleep(5000);
	}

	@When("^I Click lockicon")
	public void iClicklockicon() throws InterruptedException {
		ms.clicklockicon();
		Thread.sleep(5000);

	}

	@When("^I Click restrictionlist")
	public void iClickrestrictionlist() throws InterruptedException {
		ms.clickrestrictionlist();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@When("^I Select restriction")
	public void iClickselectrestriction() throws InterruptedException {
		ms.clickselectrestriction();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@When("^I Click apply")
	public void iClickapply() throws InterruptedException {
		ms.clickapply();
		Thread.sleep(5000);
	}

	@Then("^I Should see confirmation Text$")
	public void seetextConfirmation() {
		ms.verifytext();
		

	}
}

