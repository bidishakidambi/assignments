package PageObjects;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class editrestrictions {

	public WebDriver driver;
	By login = By.xpath("//*[@id=\"gray_link\"]");
	By username = By.xpath("//*[@id=\"username\"]");
	By submit = By.xpath("//*[@id=\"login-submit\"]");
	By password = By.xpath("//*[@id=\"password\"]");

	By profile = By.xpath("//*[@class=\"Droplist__Trigger-sc-1z05y4v-3 eteVrT\"]");

	By confluence = By.xpath("//div[@class=\"sc-dxgOiQ cWmefk\"]");
	By page = By.xpath("//*[contains(text(),'Test Page for Restriction.')]");
	By lock = By.xpath("//*[@data-test-id=\"restrictions.dialog.button\"]");
	By restriction = By.xpath("//*[@aria-label=\"open\"]");
	By selectrestriction = By.xpath("//*/span[contains(text(),\"Viewing and editing restricted\")]");
	By clickapply = By.xpath("//div[@class =\"css-vxcmzt\"]/div[1] ");
	By confirmation = By.xpath("//*/span[contains(text(),\"Only some people can view or edit.\")]");

	/**
	 * Constructor to initialize the driver
	 * 
	 * @param driver
	 */
	public editrestrictions(WebDriver driver) {
		this.driver = driver;
	}

	public void clicklogin() {
		driver.findElement(login).click();
	}

	/**
	 * Function to Enter the email address
	 * 
	 * @param task
	 */
	public void enterusername(String task) {
		driver.findElement(username).sendKeys(task, Keys.TAB);
	}

	public void enterpassword(String tasks) {
		driver.findElement(password).sendKeys(tasks, Keys.TAB);

	}

	/**
	 * Function to continue
	 * 
	 * @param task
	 */
	public void clicksubmit() {
		driver.findElement(submit).click();

	}

	public void clickprofile() {
		driver.findElement(profile).click();
		driver.findElement(profile).click();
	}

	public void clickconfluence() {
		driver.findElement(confluence).click();
	}

	public void clickpage() {
		driver.findElement(page).click();
	}

	public void clicklockicon() {
		driver.findElement(lock).click();
	}

	public void clickrestrictionlist() {
		driver.findElement(restriction).click();
	}

	public void clickselectrestriction() {
		driver.findElement(selectrestriction).click();
	}

	public void clickapply() {
		driver.findElement(clickapply).click();
	}

	public void verifytext() {
		driver.findElement(confirmation).click();

		String actualString = driver.findElement(confirmation).getText();

		Assert.assertTrue(actualString.contains("Only some people can view or edit."));
	}

}
