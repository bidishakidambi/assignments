Feature:  To verify that a user can modify restrictions on a page. 

  Scenario Outline:  to verify that a user can modify restrictions on a page. 
    Given I navigate to the <URL>
    And I Click continue
    And I Enter the username <username> 
    And I Click submit
    And I Enter the password <password>
    And I Click login
    And I Click profile
    And I Click confluence 
    And I Click page
    And I Click lockicon 
    And I Click restrictionlist
    And I Select restriction
    And I Click apply
    And I Click lockicon 
    And I Should see confirmation Text 
    
   
   
      Examples:
    | URL| username | password | ConfirmationText |
    | https://www.atlassian.com/try/cloud/signin | veda77@gmail.com | flower@1 | Only some people can view or edit. |
   
