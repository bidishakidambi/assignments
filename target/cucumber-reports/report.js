$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Restrictionpage.feature");
formatter.feature({
  "line": 1,
  "name": "To verify that a user can modify restrictions on a page.",
  "description": "",
  "id": "to-verify-that-a-user-can-modify-restrictions-on-a-page.",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "to verify that a user can modify restrictions on a page.",
  "description": "",
  "id": "to-verify-that-a-user-can-modify-restrictions-on-a-page.;to-verify-that-a-user-can-modify-restrictions-on-a-page.",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I navigate to the \u003cURL\u003e",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I Click continue",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I Enter the username \u003cusername\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I Click submit",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I Enter the password \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I Click login",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I Click profile",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I Click confluence",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I Click page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I Click lockicon",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I Click restrictionlist",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I Select restriction",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I Click apply",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I Click lockicon",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I Should see confirmation Text",
  "keyword": "And "
});
formatter.examples({
  "line": 22,
  "name": "",
  "description": "",
  "id": "to-verify-that-a-user-can-modify-restrictions-on-a-page.;to-verify-that-a-user-can-modify-restrictions-on-a-page.;",
  "rows": [
    {
      "cells": [
        "URL",
        "username",
        "password",
        "ConfirmationText"
      ],
      "line": 23,
      "id": "to-verify-that-a-user-can-modify-restrictions-on-a-page.;to-verify-that-a-user-can-modify-restrictions-on-a-page.;;1"
    },
    {
      "cells": [
        "https://www.atlassian.com/try/cloud/signin",
        "veda77@gmail.com",
        "flower@1",
        "Only some people can view or edit."
      ],
      "line": 24,
      "id": "to-verify-that-a-user-can-modify-restrictions-on-a-page.;to-verify-that-a-user-can-modify-restrictions-on-a-page.;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 10527085000,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "to verify that a user can modify restrictions on a page.",
  "description": "",
  "id": "to-verify-that-a-user-can-modify-restrictions-on-a-page.;to-verify-that-a-user-can-modify-restrictions-on-a-page.;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I navigate to the https://www.atlassian.com/try/cloud/signin",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I Click continue",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I Enter the username veda77@gmail.com",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I Click submit",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I Enter the password flower@1",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I Click login",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I Click profile",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I Click confluence",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I Click page",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I Click lockicon",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I Click restrictionlist",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I Select restriction",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I Click apply",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I Click lockicon",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I Should see confirmation Text",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.atlassian.com/try/cloud/signin",
      "offset": 18
    }
  ],
  "location": "Restrictionpage.iNavigateToThe(String)"
});
formatter.result({
  "duration": 3267396900,
  "status": "passed"
});
formatter.match({
  "location": "Restrictionpage.iClickOnlogin()"
});
formatter.result({
  "duration": 3400715600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "veda77@gmail.com",
      "offset": 21
    }
  ],
  "location": "Restrictionpage.iEnterusername(String)"
});
formatter.result({
  "duration": 434048800,
  "status": "passed"
});
formatter.match({
  "location": "Restrictionpage.iClickOnsubmit()"
});
formatter.result({
  "duration": 358229500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "flower@1",
      "offset": 21
    }
  ],
  "location": "Restrictionpage.iEnterpassword(String)"
});
formatter.result({
  "duration": 918440800,
  "status": "passed"
});
formatter.match({
  "location": "Restrictionpage.iClicklogin()"
});
formatter.result({
  "duration": 5126469600,
  "status": "passed"
});
formatter.match({
  "location": "Restrictionpage.iClickprofile()"
});
formatter.result({
  "duration": 14098760800,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d79.0.3945.117)\n  (Driver info: chromedriver\u003d79.0.3945.36 (3582db32b33893869b8c1339e8f4d9ed1816f143-refs/branch-heads/3945@{#614}),platform\u003dWindows NT 10.0.18362 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 14.09 seconds\nBuild info: version: \u00272.53.0\u0027, revision: \u002735ae25b1534ae328c771e0856c93e187490ca824\u0027, time: \u00272016-03-15 10:43:46\u0027\nSystem info: host: \u0027LAPTOP-LBB1J7O1\u0027, ip: \u002710.0.0.111\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_231\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{mobileEmulationEnabled\u003dfalse, timeouts\u003d{implicit\u003d0, pageLoad\u003d300000, script\u003d30000}, hasTouchScreen\u003dfalse, platform\u003dXP, acceptSslCerts\u003dfalse, goog:chromeOptions\u003d{debuggerAddress\u003dlocalhost:58327}, acceptInsecureCerts\u003dfalse, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, setWindowRect\u003dtrue, unexpectedAlertBehaviour\u003dignore, applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d79.0.3945.36 (3582db32b33893869b8c1339e8f4d9ed1816f143-refs/branch-heads/3945@{#614}), userDataDir\u003dC:\\Users\\ykpar\\AppData\\Local\\Temp\\scoped_dir14032_1390728171}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, strictFileInteractability\u003dfalse, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, version\u003d79.0.3945.117, browserConnectionEnabled\u003dfalse, proxy\u003d{}, nativeEvents\u003dtrue, locationContextEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue}]\nSession ID: de5e35b8b58a77d2a33ceed2cc2fd912\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\"global-nav--wac__container--profile\"]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:206)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:158)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:678)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:363)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:500)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:355)\r\n\tat PageObjects.editrestrictions.clickprofile(editrestrictions.java:86)\r\n\tat StepDefs.Restrictionpage.iClickprofile(Restrictionpage.java:73)\r\n\tat ✽.And I Click profile(Restrictionpage.feature:10)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "Restrictionpage.iClickconfluence()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Restrictionpage.iClickconpage()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Restrictionpage.iClicklockicon()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Restrictionpage.iClickrestrictionlist()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Restrictionpage.iClickselectrestriction()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Restrictionpage.iClickapply()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Restrictionpage.iClicklockicon()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Restrictionpage.seetextConfirmation()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 837614200,
  "status": "passed"
});
});